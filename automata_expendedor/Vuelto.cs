﻿using System;
using System.Collections;
namespace automata_expendedor
{
    public class Vuelto
    {


        //variables
        private int cantidadVuelto;
        private readonly Queue EntradaInicial;
        private readonly bool HayQueCalcularVuelto;
        private readonly ArrayList caja, lexema;

        //funciones
        private bool AlcanzanBilletes(int billete, int cantidad) => (cantidad != 0) && ((int)caja[billete] >= cantidad);
        private void TomarBilletes(int cantidad1, int cantidad5, int cantidad10, int cantidad20)
        {
            if (AlcanzanBilletes(Utils.VEINTE, cantidad20)) { caja[Utils.VEINTE] = ((int)caja[Utils.VEINTE]) - cantidad20; }
                else if (AlcanzanBilletes(Utils.DIEZ, cantidad10)) { caja[Utils.DIEZ]  = ((int)caja[Utils.DIEZ])  - cantidad10; }
                else if (AlcanzanBilletes(Utils.CINCO, cantidad5)) { caja[Utils.CINCO] = ((int)caja[Utils.CINCO]) - cantidad5;  }
                else if (AlcanzanBilletes(Utils.UNO, cantidad1))   { caja[Utils.UNO]   = ((int)caja[Utils.UNO])   - cantidad1;  }
                else { throw new Exception("Imposible dar vuelto"); }

            cantidadVuelto -= cantidad1;//cantidad1 porque tiene el valor del bille por ser la unidad
        }
        private void AgregarBilletesLexema()
        {
            foreach (int caracter in lexema)
            {
                switch (caracter)
                {
                    case 1: caja[Utils.UNO] = ((int)caja[Utils.UNO]) + 1; break;
                    case 5: caja[Utils.CINCO] = ((int)caja[Utils.CINCO]) + 1; break;
                    case 10: caja[Utils.DIEZ] = ((int)caja[Utils.DIEZ]) + 1; break;
                    case 20: caja[Utils.VEINTE] = ((int)caja[Utils.VEINTE]) + 1; break;
                    default: throw new Exception("Problema con lexema");
                }

            }
        }
        private void RestarMercaderia()
        {
            ArrayList chelas = Utils.LeerData("mercaderia.csv");
            int chela = Utils.CHELASELECCIONADA;
            chelas[chela] = (int)chelas[chela] - 1;
            Utils.EscribirData(chelas, "mercaderia.csv");

        }
        private Queue CompraRealizada(ArrayList vueltoCadena)
        {

            vueltoCadena.Reverse();
            AgregarBilletesLexema();
            RestarMercaderia();

            Utils.EscribirData(caja, "caja.csv");
            return new Queue(vueltoCadena);
        }
        private Queue ObtenerBilletesVuelto()
        {
            try
            {
                ArrayList vueltoCadena = new ArrayList();
                do
                {
                    //tomarBilletes(1,5,10,20)
                    if (cantidadVuelto >= 20) { TomarBilletes(20, 4, 2, 1); vueltoCadena.Add(20); }
                    else if (cantidadVuelto >= 10) { TomarBilletes(10, 2, 1, 0); vueltoCadena.Add(10); }
                    else if (cantidadVuelto >=  5) { TomarBilletes( 5,  1, 0, 0); vueltoCadena.Add(5); }
                    else if (cantidadVuelto >=  1) { TomarBilletes( 1,  0, 0, 0); vueltoCadena.Add(1); }

                } while (cantidadVuelto != 0);

                return CompraRealizada(vueltoCadena);
            }
            catch (Exception ex) { Utils.Mensaje(ex.Message, false); return EntradaInicial; }

        }
        public Queue Calculo()
        {
            if (HayQueCalcularVuelto) { return (cantidadVuelto > 0) ? ObtenerBilletesVuelto() : new Queue(); }
            return EntradaInicial;
        }

        //contructor
        public Vuelto(int cantidadVuelto, Queue EntradaInicial, ArrayList lexema, bool HayQueCalcularVuelto)
        {
            this.cantidadVuelto = cantidadVuelto;
            this.EntradaInicial = EntradaInicial;
            this.lexema = lexema;
            this.HayQueCalcularVuelto = HayQueCalcularVuelto;
            this.caja = Utils.LeerData("caja.csv");
        }
    }
}
