﻿using System;
using System.Collections;
using Gtk;
using System.IO;

namespace automata_expendedor
{
    public class Utils
    {
        //constantes
        public const int UNO = 0, CINCO = 1, DIEZ = 2, VEINTE = 3,
                         CABRO = 0, GALLO = 1, STELLA = 2, BUDW = 3,CORONA = 4, 
                         BRAHVA = 5, PILSNR = 6, TECATE = 7, BALTIKA = 8, MONTEC = 9;
        public static int CHELASELECCIONADA;

        // funciones estaticas
        public static void Mensaje(string txt,  bool estado)
        {
            var tipo = (!estado) ? MessageType.Error: MessageType.Other;
            MessageDialog md = new MessageDialog(null, DialogFlags.Modal, tipo, ButtonsType.Ok, txt);
            md.Run(); md.Destroy();
        }
        public static int SumaCola(Queue cola)
        {
            int saldo = 0;
            foreach (int valor in cola){saldo += valor;}
            return saldo;
        }
        public static void EscribirData(ArrayList datos, string nombreArchivo)
        {
            string csv = "";
            foreach (int dato in datos) { csv += dato + ","; }
            csv = csv.Remove(csv.Length - 1);
            TextWriter tw = new StreamWriter(nombreArchivo);
            tw.Write(csv); tw.Close();
        }
        public static ArrayList LeerData(string nombreArchivo)
        {
            TextReader sr = new StreamReader(nombreArchivo);
            //string -> string[] -> int[] -> arraylist
            ArrayList returnedArray = new ArrayList(Array.ConvertAll(sr.ReadLine().Split(','), s => int.Parse(s)));
            sr.Close();
            return returnedArray;
        }
    }
}
