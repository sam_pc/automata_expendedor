﻿using System;
using System.Collections;
using automata_expendedor;
using Gtk;
public partial class MainWindow : Gtk.Window
{
    //variables
    private Queue entrada = new Queue();

    // funciones
    private void CambiarTextoChelas(Gtk.Label ch, int existencia, string nombre, int precio)
    {
        ch.Text = nombre + " - " + existencia + "u\nQ." + precio.ToString() + ".00";
    }
    private string DistribucionVuelto(Queue cola)
    {
        string elementos = "";
        foreach (int item in cola) { elementos += item.ToString() + " - ";}
        return elementos.Remove(elementos.Length - 2);
    }
    private void ActualizaMercaderia()
    {
        ArrayList existencias = Utils.LeerData("mercaderia.csv");
        for (int i = 0; i < existencias.Count; i++)
        {
            switch (i)
            {
                case Utils.CABRO: CambiarTextoChelas(ch1l, (int)existencias[i], "Cabro", 8); break;
                case Utils.GALLO: CambiarTextoChelas(ch2l, (int)existencias[i], "Gallo", 7); break;
                case Utils.STELLA: CambiarTextoChelas(ch3l, (int)existencias[i], "Stella", 15); break;
                case Utils.BUDW: CambiarTextoChelas(ch4l, (int)existencias[i], "BudW", 18); break;
                case Utils.CORONA: CambiarTextoChelas(ch5l, (int)existencias[i], "Corona", 9); break;
                case Utils.BRAHVA: CambiarTextoChelas(ch6l, (int)existencias[i], "Brahva", 6); break;
                case Utils.PILSNR: CambiarTextoChelas(ch7l, (int)existencias[i], "Pilsnr", 6); break;
                case Utils.TECATE: CambiarTextoChelas(ch8l, (int)existencias[i], "Tecate", 5); break;
                case Utils.BALTIKA: CambiarTextoChelas(ch9l, (int)existencias[i], "Baltika", 18); break;
                case Utils.MONTEC: CambiarTextoChelas(ch10l, (int)existencias[i], "MonteC", 16); break;
            }
        }
    }
    private void ActualizaSaldo() => saldol.Markup = "<span size='25000'> Q." + Utils.SumaCola(entrada) + ".00 \n Saldo</span>";
    private void DineroClick(int valor){ entrada.Enqueue(valor); ActualizaSaldo();}
    private bool ExistenciaChelaSeleccionada()
    {
        ArrayList caja = Utils.LeerData("caja.csv");
        return ((int)caja[Utils.CHELASELECCIONADA] > 0);
    }
    private void ChelaClick(string estadoAceptador, int chelaSeleccionada)
    {
        if (ExistenciaChelaSeleccionada())
        {
            Utils.CHELASELECCIONADA = chelaSeleccionada;
            entrada = new Automata(entrada, estadoAceptador).Iniciar();
            ActualizaSaldo();
            ActualizaMercaderia();

        }
        else { Utils.Mensaje("Lo sentimos no hay chelas :(", false); }

    } 

    // eventos
    protected void OnCh1Clicked(object sender, EventArgs e) => ChelaClick("q8", Utils.CABRO);
    protected void OnCh2Clicked(object sender, EventArgs e) => ChelaClick("q7", Utils.GALLO);
    protected void OnCh3Clicked(object sender, EventArgs e) => ChelaClick("q15", Utils.STELLA);
    protected void OnCh4Clicked(object sender, EventArgs e) => ChelaClick("q18", Utils.BUDW);
    protected void OnCh5Clicked(object sender, EventArgs e) => ChelaClick("q9", Utils.CORONA);
    protected void OnCh6Clicked(object sender, EventArgs e) => ChelaClick("q6", Utils.BRAHVA);
    protected void OnCh7Clicked(object sender, EventArgs e) => ChelaClick("q6", Utils.PILSNR);
    protected void OnCh8Clicked(object sender, EventArgs e) => ChelaClick("q5", Utils.TECATE);
    protected void OnCh9Clicked(object sender, EventArgs e) => ChelaClick("q18", Utils.BALTIKA);
    protected void OnCh10Clicked(object sender, EventArgs e) => ChelaClick("q16", Utils.MONTEC);

    protected void OnBllt1Clicked(object sender, EventArgs e) => DineroClick(1);
    protected void OnBllt5Clicked(object sender, EventArgs e) => DineroClick(5);
    protected void OnBllt10Clicked(object sender, EventArgs e) => DineroClick(10);
    protected void OnBllt20Clicked(object sender, EventArgs e) => DineroClick(20);

   
    protected void OnPedirVueltoClicked(object sender, EventArgs e)
    {
        if (entrada.Count > 0)
        {
            Utils.Mensaje("Despachado Q." + Utils.SumaCola(entrada)+".00 -  Tus billetes: "+DistribucionVuelto(entrada), true);
            entrada = new Queue(); ActualizaSaldo();
        }
        else { Utils.Mensaje("Tamos cabales, no hay vuelto", false); }

    }
    protected void OnDeleteEvent(object sender, DeleteEventArgs a) { Application.Quit(); a.RetVal = true; }

    //constructor
    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();
        label7.Markup = "<span size='20000'> Selecciona tu Chela</span>";
        label14.Markup = "<span size='20000'> Ingrese capital a chupar</span>";
        ActualizaSaldo();
        ActualizaMercaderia();
    }

}
