﻿using System;
using System.Collections;

namespace automata_expendedor
{
    public class Automata
    {
        //variables
        private Queue entrada, entradaAux;
        private ArrayList lexema = new ArrayList();
        private string estadoAceptador = "", estadoActual="q0";

        //funciones
        private int LexemaAntesFinal()
        {
            int suma = 0;
            for (int i = 0; i < lexema.Count - 1; i++) { suma += (int)lexema[i]; }
            return suma;
        }
        private int TotalVuelto()
        {
            int actual = int.Parse(estadoActual.Substring(1));
            int aceptado = int.Parse(estadoAceptador.Substring(1));
            int totalVuelto = actual - aceptado + Utils.SumaCola(entrada);
            return (actual != 20) ? totalVuelto : totalVuelto + LexemaAntesFinal();
        }
        private void EsEstadoActual(string estado1, string estado5, string estado10)
        {
            // si el estado actual aun no sobrepasa el estado aceptador
            if ( int.Parse(estadoActual.Substring(1)) < int.Parse(estadoAceptador.Substring(1)))
            {
                if(entrada.Count > 0)
                {
                    int caracter = (int) entrada.Dequeue();
                    switch (caracter)
                    {
                        case 1: estadoActual = estado1; break;
                        case 5: estadoActual = estado5; break;
                        case 10: estadoActual = estado10; break;
                        case 20: estadoActual = "q20"; break;
                        default: throw new Exception("2");
                    }
                    lexema.Add(caracter);
                }
            }
            else { 

                throw new Exception("1"); 
            } // llego ha estado de aceptacion
        }
        private void  AlgoritmoAutomata() 
        {
            for (int i = 0; i <= entradaAux.Count; i++)
            {
                switch (estadoActual)
                {
                    case "q0": EsEstadoActual("q1", "q5", "q10"); break;
                    case "q1": EsEstadoActual("q2", "q6", "q11"); break;
                    case "q2": EsEstadoActual("q3", "q7", "q12"); break;
                    case "q3": EsEstadoActual("q4", "q8", "q13"); break;
                    case "q4": EsEstadoActual("q5", "q9", "q14"); break;
                    case "q5": EsEstadoActual("q6", "q10", "q15"); break;
                    case "q6": EsEstadoActual("q7", "q11", "q16"); break;
                    case "q7": EsEstadoActual("q8", "q12", "q17"); break;
                    case "q8": EsEstadoActual("q9", "q13", "q18"); break;
                    case "q9": EsEstadoActual("q10", "q14", "q19"); break;
                    case "q10": EsEstadoActual("q11", "q15", "q20"); break;
                    case "q11": EsEstadoActual("q12", "q16", "q20"); break;
                    case "q12": EsEstadoActual("q13", "q17", "q20"); break;
                    case "q13": EsEstadoActual("q14", "q18", "q20"); break;
                    case "q14": EsEstadoActual("q15", "q19", "q20"); break;
                    case "q15": EsEstadoActual("q16", "q20", "q20"); break;
                    case "q16": EsEstadoActual("q17", "q20", "q20"); break;
                    case "q17": EsEstadoActual("q18", "q20", "q20"); break;
                    case "q18": EsEstadoActual("q19", "q20", "q20"); break;
                    case "q19": EsEstadoActual("q20", "q20", "q20"); break;
                    case "q20": EsEstadoActual("q20", "q20", "q20"); break;
                }
            }
            // no llego a estado de aceptacion
            throw new Exception("0");
        }
        public Queue Iniciar()
        {
            try
            {
                if (entrada.Count > 0) { AlgoritmoAutomata(); throw new Exception("1"); } //excepcion obigatoria para el metodo
                else { throw new Exception("0"); }
            }
            catch (Exception ex)
            {
                switch (ex.Message)
                {
                    // dependiendo de la finalizacion envia a modulo de vuelto con calculo o no
                    case "0":
                        Utils.Mensaje("No tienes dinero suficiente", false);
                        return new Vuelto(0, entradaAux, lexema, false).Calculo();

                    case "1":
                        Utils.Mensaje("Dispensando Chela... SALUD!", true);
                        return new Vuelto(TotalVuelto(), entradaAux, lexema, true).Calculo();

                    case "2":
                        Utils.Mensaje("Error en la cadena", false);
                        return new Vuelto(0, entradaAux, lexema, false).Calculo();

                    default:
                        Utils.Mensaje("Error externo", false);
                        return new Vuelto(0, entradaAux, lexema, false).Calculo();

                }
            }
        }

        // constructor
        public Automata(Queue entrada, string estadoAceptador)
        {
            entradaAux = new Queue(entrada);
            this.entrada = entrada;
            this.estadoAceptador = estadoAceptador;
        }
    }
}
